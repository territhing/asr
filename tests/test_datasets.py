import os
import pathlib

import pytest
import tokenizers
import torch.utils.data

import asr.dataset

RESOURCE_DIR = pathlib.Path(__file__).resolve().parent / "resources"
os.environ["TOKENIZERS_PARALLELISM"] = "false"


@pytest.mark.skipif(
    not RESOURCE_DIR.exists(), reason="could not find the directory with resources"
)
class TestDatasetProcessing:
    @pytest.mark.skipif(
        not (RESOURCE_DIR / "LibriSpeech").exists(),
        reason="could not find data from LibriSpeech",
    )
    @pytest.mark.skipif(
        not (RESOURCE_DIR / "LibriSpeech" / "dev-clean").exists(),
        reason="could not find dev-clean from LibriSpeech",
    )
    def test_librispeech_processing(self):
        tokenizer = tokenizers.Tokenizer.from_pretrained("bert-base-uncased")
        dataset = asr.dataset.LibriSpeechDataset(RESOURCE_DIR, url="dev-clean")
        dataloader = torch.utils.data.DataLoader(
            dataset,
            shuffle=True,
            batch_size=128,
            num_workers=4,
            collate_fn=asr.dataset.DataCollatorWithPadding(tokenizer),
        )

        # we only test that data collator works fine
        for _ in dataloader:
            pass

    @pytest.mark.skipif(
        not (RESOURCE_DIR / "LJSpeech-1.1").exists(),
        reason="could not find data from LJSpeech",
    )
    def test_ljspeech_processing(self):
        tokenizer = tokenizers.Tokenizer.from_pretrained("bert-base-uncased")
        dataset = asr.dataset.LJSpeechDataset(RESOURCE_DIR)
        dataloader = torch.utils.data.DataLoader(
            dataset,
            shuffle=True,
            batch_size=128,
            num_workers=4,
            collate_fn=asr.dataset.DataCollatorWithPadding(tokenizer),
        )

        # we only test that data collator works fine
        for _ in dataloader:
            pass

    @pytest.mark.skipif(
        not (RESOURCE_DIR / "CommonVoice").exists(),
        reason="could not find data from CommonVoice",
    )
    def test_common_voice_processing(self):
        tokenizer = tokenizers.Tokenizer.from_pretrained("bert-base-uncased")
        dataset = asr.dataset.LJSpeechDataset(RESOURCE_DIR / "CommonVoice")
        dataloader = torch.utils.data.DataLoader(
            dataset,
            shuffle=True,
            batch_size=128,
            num_workers=4,
            collate_fn=asr.dataset.DataCollatorWithPadding(tokenizer),
        )

        # we only test that data collator works fine
        for _ in dataloader:
            pass
