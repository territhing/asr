import abc
import dataclasses
import pathlib
from typing import Union

import tokenizers
import torch.utils.data
import torchaudio.datasets


@dataclasses.dataclass
class Entry:
    waveform: torch.Tensor
    sample_rate: int
    transcript: str


@dataclasses.dataclass
class Batch:
    waveforms: torch.Tensor
    sample_rates: torch.Tensor
    transcripts: torch.Tensor


class ASRDataset(abc.ABC, torch.utils.data.Dataset):
    @abc.abstractmethod
    def __getitem__(self, idx: int) -> Entry:
        raise NotImplementedError

    @abc.abstractmethod
    def __len__(self) -> int:
        raise NotImplementedError


class LibriSpeechDataset(ASRDataset):
    def __init__(
        self,
        root: Union[str, pathlib.Path],
        url: str = "train-clean-100",
        folder_in_archive: str = "LibriSpeech",
        download: bool = False,
    ):
        self.__dataset = torchaudio.datasets.LIBRISPEECH(
            root, url, folder_in_archive, download
        )

    def __getitem__(self, idx: int) -> Entry:
        waveform, sample_rate, transcript, _, _, _ = self.__dataset[idx]
        return Entry(waveform, sample_rate, transcript)

    def __len__(self) -> int:
        return len(self.__dataset)


class LJSpeechDataset(ASRDataset):
    def __init__(
        self,
        root: Union[str, pathlib.Path],
        url: str = "https://data.keithito.com/data/speech/LJSpeech-1.1.tar.bz2",
        folder_in_archive: str = "wavs",
        download: bool = False,
    ):
        self.__dataset = torchaudio.datasets.LJSPEECH(
            root, url, folder_in_archive, download
        )

    def __getitem__(self, idx: int) -> Entry:
        waveform, sample_rate, transcript, _ = self.__dataset[idx]
        return Entry(waveform, sample_rate, transcript)

    def __len__(self) -> int:
        return len(self.__dataset)


class CommonVoiceDataset(ASRDataset):
    def __init__(
        self,
        root: Union[str, pathlib.Path],
        tsv: str = "train.tsv",
        download: bool = False,
    ):
        self.__dataset = torchaudio.datasets.COMMONVOICE(root, tsv)

    def __getitem__(self, idx: int) -> Entry:
        waveform, sample_rate, dictionary = self.__dataset[idx]
        return Entry(waveform, sample_rate, dictionary["sentence"])

    def __len__(self) -> int:
        return len(self.__dataset)


class DataCollatorWithPadding:
    def __init__(
        self,
        tokenizer: tokenizers.Tokenizer,
        *,
        batch_first: bool = True,
        padding_value: float = 0.0
    ):
        self.__tokenizer = tokenizer
        self.__batch_first = batch_first
        self.__padding_value = padding_value
        self.__tokenizer.enable_padding(direction="right")

    def __call__(self, data: list[Entry]) -> Batch:
        waveforms: list[torch.Tensor] = []
        sample_rates: list[int] = []
        transcripts: list[str] = []

        for entry in data:
            waveforms.append(torch.flatten(entry.waveform))
            sample_rates.append(entry.sample_rate)
            transcripts.append(entry.transcript)

        input_ids = torch.tensor(
            [encoding.ids for encoding in self.__tokenizer.encode_batch(transcripts)]
        )

        return Batch(
            torch.nn.utils.rnn.pad_sequence(
                waveforms,
                batch_first=self.__batch_first,
                padding_value=self.__padding_value,
            ),
            torch.unsqueeze(torch.tensor(sample_rates), 1),
            input_ids,
        )
